#ifndef FREQUENCIES_H_INCLUDED
#define FREQUENCIES_H_INCLUDED

#define B1_NATURAL_INDEX 0
#define C2_NATURAL_INDEX 1
#define C2_SHARP_INDEX 2
#define D2_NATURAL_INDEX 3
#define D2_SHARP_INDEX 4
#define E2_NATURAL_INDEX 5
#define F2_NATURAL_INDEX 6
#define F2_SHARP_INDEX 7
#define G2_NATURAL_INDEX 8
#define G2_SHARP_INDEX 9
#define A2_NATURAL_INDEX 10
#define A2_SHARP_INDEX 11
#define B2_NATURAL_INDEX 12
#define C3_NATURAL_INDEX 13
#define C3_SHARP_INDEX 14
#define D3_NATURAL_INDEX 15
#define D3_SHARP_INDEX 16
#define E3_NATURAL_INDEX 17
#define F3_NATURAL_INDEX 18
#define F3_SHARP_INDEX 19
#define G3_NATURAL_INDEX 20
#define G3_SHARP_INDEX 21
#define A3_NATURAL_INDEX 22
#define A3_SHARP_INDEX 23
#define B3_NATURAL_INDEX 24
#define C4_NATURAL_INDEX 25
#define C4_SHARP_INDEX 26
#define D4_NATURAL_INDEX 27
#define D4_SHARP_INDEX 28
#define E4_NATURAL_INDEX 29

#define MAX_FREQUENCY_INDEX 29

#define B1_NATURAL_FREQUENCY 61.7
#define C2_NATURAL_FREQUENCY 65.4
#define C2_SHARP_FREQUENCY 69.3
#define D2_NATURAL_FREQUENCY 73.4
#define D2_SHARP_FREQUENCY 77.8
#define E2_NATURAL_FREQUENCY 82.4
#define F2_NATURAL_FREQUENCY 87.3
#define F2_SHARP_FREQUENCY 92.5
#define G2_NATURAL_FREQUENCY 98.0
#define G2_SHARP_FREQUENCY 103.8
#define A2_NATURAL_FREQUENCY 110.0
#define A2_SHARP_FREQUENCY 116.5
#define B2_NATURAL_FREQUENCY 123.5
#define C3_NATURAL_FREQUENCY 130.8
#define C3_SHARP_FREQUENCY 138.6
#define D3_NATURAL_FREQUENCY 146.8
#define D3_SHARP_FREQUENCY 155.6
#define E3_NATURAL_FREQUENCY 164.8
#define F3_NATURAL_FREQUENCY 174.6
#define F3_SHARP_FREQUENCY 185.0
#define G3_NATURAL_FREQUENCY 196.0
#define G3_SHARP_FREQUENCY 207.7
#define A3_NATURAL_FREQUENCY 220.0
#define A3_SHARP_FREQUENCY 233.1
#define B3_NATURAL_FREQUENCY 246.9
#define C4_NATURAL_FREQUENCY 261.6
#define C4_SHARP_FREQUENCY 277.2
#define D4_NATURAL_FREQUENCY 293.7
#define D4_SHARP_FREQUENCY 311.1
#define E4_NATURAL_FREQUENCY 330.0

float frequencies[30] = {
    B1_NATURAL_FREQUENCY,
    C2_NATURAL_FREQUENCY,
    C2_SHARP_FREQUENCY,
    D2_NATURAL_FREQUENCY,
    D2_SHARP_FREQUENCY,
    E2_NATURAL_FREQUENCY,
    F2_NATURAL_FREQUENCY,
    F2_SHARP_FREQUENCY,
    G2_NATURAL_FREQUENCY,
    G2_SHARP_FREQUENCY,
    A2_NATURAL_FREQUENCY,
    A2_SHARP_FREQUENCY,
    B2_NATURAL_FREQUENCY,
    C3_NATURAL_FREQUENCY,
    C3_SHARP_FREQUENCY,
    D3_NATURAL_FREQUENCY,
    D3_SHARP_FREQUENCY,
    E3_NATURAL_FREQUENCY,
    F3_NATURAL_FREQUENCY,
    F3_SHARP_FREQUENCY,
    G3_NATURAL_FREQUENCY,
    G3_SHARP_FREQUENCY,
    A3_NATURAL_FREQUENCY,
    A3_SHARP_FREQUENCY,
    B3_NATURAL_FREQUENCY,
    C4_NATURAL_FREQUENCY,
    C4_SHARP_FREQUENCY,
    D4_NATURAL_FREQUENCY,
    D4_SHARP_FREQUENCY,
    E4_NATURAL_FREQUENCY,
};

char freqLetters[30] = {
    'B',
    'C',
    'C',
    'D',
    'D',
    'E',
    'F',
    'F',
    'G',
    'G',
    'A',
    'A',
    'B',
    'C',
    'C',
    'D',
    'D',
    'E',
    'F',
    'F',
    'G',
    'G',
    'A',
    'A',
    'B',
    'C',
    'C',
    'D',
    'D',
    'E',
};

char freqAccidentals[30] = {
    ' ',
    ' ',
    '#',
    ' ',
    '#',
    ' ',
    ' ',
    '#',
    ' ',
    '#',
    ' ',
    '#',
    ' ',
    ' ',
    '#',
    ' ',
    '#',
    ' ',
    ' ',
    '#',
    ' ',
    '#',
    ' ',
    '#',
    ' ',
    ' ',
    '#',
    ' ',
    '#',
    ' ',
};

char freqOctaves[30] = {
    '1',
    '2',
    '2',
    '2',
    '2',
    '2',
    '2',
    '2',
    '2',
    '2',
    '2',
    '2',
    '2',
    '3',
    '3',
    '3',
    '3',
    '3',
    '3',
    '3',
    '3',
    '3',
    '3',
    '3',
    '3',
    '4',
    '4',
    '4',
    '4',
    '4',
};

#endif // FREQUENCIES_H_INCLUDED
