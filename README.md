# Open Source Automatic Guitar Tuner

![CAD Render](media/rev2_render.JPG)

This project provides source code and 3D-printable files for an automatic guitar tuner based off Band Industries' [Roadie 3](https://www.roadiemusic.com/roadie3). The source code in this project is licensed under the 3-clause BSD license. **No further updates planned for this project**

## As Seen In
[![The MagPi Magazine Issue 111](media/The-MagPi-Issue-111-November-2021_cover.jpg)](https://store.rpipress.cc/products/the-magpi-magazine-111)

[![Hackster IO](media/hackster_logo.jpg)](https://www.hackster.io/news/guyrandy-jean-gilles-raspberry-pi-pico-powered-automated-guitar-tuner-gets-you-pitch-perfect-fast-81c2bbf02b1d)

[![Tom's Hardware](media/tomshardware.png)](https://www.tomshardware.com/news/raspberry-pi-pico-automatic-guitar-tuner)

[![Raspberry Pi](media/Raspberry_Pi_logo.png)](https://www.raspberrypi.org/blog/automatically-tune-your-guitar-with-raspberry-pi-pico/)

[![Hackaday](media/hackaday-logo.png)](https://hackaday.com/2021/09/24/handheld-bot-takes-the-tedium-out-of-guitar-tuning/)

## Note
1. ~~While the project is functional, I couldn't get the note detection to work perfectly and the motor I used struggled at times to turn my guitar's tuning pegs. Again, it's functional so I have no plans to optimize the project, but maybe you'll find use in what I've done so far.~~ This is the second revision. Find the first iteration here: https://gitlab.com/guyjeangilles/automatic-guitar-tuner/-/tree/b91588dfc0f9ad3a285ed1cc72bed9c4d2ef4cef

1. [The Raspberry Pi Pico SDK](https://github.com/raspberrypi/pico-sdk) is a submodule of this repo. Access to the SDK is required to build this project.

## Changes in Revision 2
- [X] Alternate tunings
- [X] Always tune strings up to their target frequency
- [X] Optional 1/4" input

## Use Instructions
Push the "pitch increase" or "pitch decrease" buttons to select the target note. The corresponding note will appear on the LCD screen. Once the desired tone is selected, place the tuner's head on the guitar's tuning peg. Pluck the desired string then press **and** hold the push button to enabled the microphone. If the signal quality is good, a "#" or "*b*" symbol will appear on the LCD with a corresponding bar indicating how out of tune the string is. Wait until "Done" is displayed on the LCD. The string is now tuned.
