#include <math.h>
#include "nokia5110.h"
#include "display.h"
#include "pitchSelection.h"

// Private functions
void setDisplaySharpDelta(float delta){
    uint8_t maxRectWidth = 33;
    float maxFreqDelta = getSharpFreq() - getTargetFreq();
    uint8_t rectWidth = maxRectWidth * (uint8_t)delta/maxFreqDelta;
    setCursor(44, 35);
    setTextSize(2);
    printString("#");

    fillRect(55, 40, rectWidth, 4, BLACK);
}

void setDisplayFlatDelta(float delta){
    uint8_t maxRectWidth = 32;
    float maxFreqDelta = getTargetFreq() - getFlatFreq();
    uint8_t rectWidth = maxRectWidth * fabs(delta/maxFreqDelta);
    setCursor(33, 35);
    setTextSize(2);
    printString("b");

    fillRect(maxRectWidth - rectWidth, 40, rectWidth, 4, BLACK);
}

// public functions

void setDisplayTargetFreq(uint8_t freqIdx){

    char letter [3] = {getCurrentPitchLetter(freqIdx), getCurrentPitchAccidental(freqIdx), '\0'};
    uint8_t letter_x = 25;
    uint8_t octave_x = 57;
    if (letter[1] == ' '){ // a natural tone
        letter_x = 33;
        octave_x = 50;
    }

    setCursor(letter_x, 4);
    setTextSize(3);
    printString(letter);

    setCursor(octave_x, 24);
    setTextSize(1);
    char octave [2] = {getCurrentPitchOctave(freqIdx), '\0'};
    printString(octave);
}

void setDisplayPitchDelta(float delta){
    if (delta >= 0) {
        setDisplaySharpDelta(delta);
    } else {
        setDisplayFlatDelta(delta);
    }
}

void setDisplayPoorSignal(void){
    setCursor(10, 35);
    setTextSize(1);
    printString("Poor Signal");
}

void setDisplayNotListening(void){
    setCursor(0, 35);
    setTextSize(1);
    printString("Not Listening");
}

void setDisplayDoneStatus(void){
    setCursor(0, 35);
    setTextSize(1);
    char done[] = {'\1', '\1', '\1', '\1', ' ', 'D', 'o', 'n', 'e', ' ', '\1', '\1', '\1', '\1', '\0' };
    printString(done);
}

void eraseDisplay(void){
    clearDisplay();
}

void displayInit(void){
    Nokia5110_Init();
}

void showDisplay(void){
    display();
}
