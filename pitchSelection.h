#ifndef PITCHSELECTION_INCLUDED
#define PITCHSELECTION_INCLUDED

void pitchSelectionInit(void);
float getTargetFreq(void);
void setTargetFreq(void);
float getFlatFreq(void);
float getSharpFreq(void);
char getCurrentPitchLetter(uint8_t freqIdx);
char getCurrentPitchOctave(uint8_t freqIdx);
char getCurrentPitchAccidental(uint8_t freqIdx);
uint8_t getCurrentFreqIdx(void);


#endif // PITCHSELECTION_INCLUDED
