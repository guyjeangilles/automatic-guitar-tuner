#ifndef DISPLAY_H_INCLUDED
#define DISPLAY_H_INCLUDED

void displayInit(void);
void setDisplayTargetFreq(uint8_t freqIdx);
void setDisplayPitchDelta(float delta);
void setDisplayNotListening(void);
void setDisplayPoorSignal(void);
void setDisplayDoneStatus(void);
void eraseDisplay(void);
void showDisplay(void);


#endif // DISPLAY_H_INCLUDED
