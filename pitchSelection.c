#include "pico/stdlib.h"
#include "hardware/gpio.h"
#include "pitchSelection.h"
#include "frequencies.h"

#define PITCH_DECREASE_PIN 21
#define PITCH_INCREASE_PIN 22

int8_t currentFreqIdx;

void pitchSelectionInit(void){
    gpio_init(PITCH_DECREASE_PIN);
    gpio_init(PITCH_INCREASE_PIN);
    gpio_set_dir(PITCH_DECREASE_PIN, GPIO_IN);
    gpio_set_dir(PITCH_INCREASE_PIN, GPIO_IN);

    currentFreqIdx = E4_NATURAL_INDEX;
}

void setTargetFreq(void){
    if (gpio_get(PITCH_DECREASE_PIN)){ // todo interrupt?
        sleep_ms(200);
        currentFreqIdx++;
    } else if (gpio_get(PITCH_INCREASE_PIN)){
        sleep_ms(200);
        currentFreqIdx--;
    }
    if (currentFreqIdx > MAX_FREQUENCY_INDEX){
        currentFreqIdx = 0;
    } else if (currentFreqIdx < 0) {
        currentFreqIdx = MAX_FREQUENCY_INDEX;
    }
}

float getTargetFreq(void){
    return frequencies[currentFreqIdx];
}

float getFlatFreq(){
    if (currentFreqIdx == 0){
        return 55.0;
    }

    uint8_t flatFreqIdx = currentFreqIdx - 1;
    return frequencies[flatFreqIdx];
}

float getSharpFreq(){
    if (currentFreqIdx == MAX_FREQUENCY_INDEX){
        return 349.0;
    }

    uint8_t sharpFreqIdx = currentFreqIdx + 1;
    return frequencies[sharpFreqIdx];
}

char getCurrentPitchLetter(uint8_t freqIdx){
    return freqLetters[freqIdx];
}

char getCurrentPitchOctave(uint8_t freqIdx){
    return freqOctaves[freqIdx];
}

char getCurrentPitchAccidental(uint8_t freqIdx){
    return freqAccidentals[freqIdx];
}

uint8_t getCurrentFreqIdx(void){
    return currentFreqIdx;
}
